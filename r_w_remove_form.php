<!DOCTYPE html>
<html>
  <head>
    <title>Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="routines.php">Routines<span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class="col-10">
           <form action="list_workouts.php" method="get" id="form3">
            <?php
                $this_rid = $_GET["rmworkout"];
                echo "<input type='hidden' name='workout' value='"."$this_rid"."'>";
            ?>
                <td><button type="submit" class="btn btn-secondary" id="back">Back</button></td>
		   </form>
           <br><br>
           <h3>Remove Workouts from Routine <?php
            $this_rid = $_GET["rmworkout"];
            $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
            mysqli_select_db($link, 'bjanczuk');
            $query = "SELECT name FROM Routines where rid='"."$this_rid"."'";
            $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));

            while($tuple = mysqli_fetch_assoc($result)) {
                echo " ";
                foreach ($tuple as $col_key => $col_val) {
                    echo "$col_val";
                }
            }
  
           ?></h3>
        </div>
      </div>
      <br> 
            <div>
                <h4>Workouts to Remove:</h4>
            </div>
            <br>
            <div class="row">
             <div class="col-sm-4 form-group">
                <?php
                  $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or die ('Database connection error');
                  mysqli_select_db($link, 'bjanczuk');

                  $this_rid = $_GET["rmworkout"];
                  $query1 = "SELECT wid, weekdays FROM WorkoutsInRoutines WHERE rid="."$this_rid";
                  $result1 = mysqli_query($link, $query1) or die('Query failed'.mysqli_error($link));
                  $included = array();
                  if ($result1->num_rows>0) {
                      while ($tuple = mysqli_fetch_assoc($result1)) {
                          foreach($tuple as $col_val) {
                              array_push($included, $col_val);
                          }
                      }
                  

                  echo "<table class='table table-bordered'>
                          <thead class='thead-dark'>
                            <tr>
                              <th>Workout Name</th>
                              <th>Days of the Week</th>
                              <th>Delete?</th>
                            </tr>
                          </thead>
                          <tbody>";
                  foreach ($included as $key => $value) {
                        if ($key%2 == 0) {
                            $query2 = "SELECT name FROM GeneralWorkouts WHERE wid=".$value."";
                            $result2 = mysqli_query($link, $query2) or die('Query Failed'.mysqli_error($link));

                            if ($result2->num_rows>0) {
                             while ($tuple = mysqli_fetch_assoc($result2)) {
                                foreach($tuple as $col_val) {
                                    $workout_name = $col_val;
                                }
                             }
                            }
                            echo "<tr>
                                    <form action='delete_workout.php' method='get' id='rmForm'>
                                      <input type='hidden' name='delworkout' value='".$value."'>
                                      <input type='hidden' name='routine_name' value='".$this_rid."'>
                                      <input type='hidden' name='days' value='".$included[$key+1]."'>
                                      <td>".$workout_name."</td>
                                      <td>".$included[$key+1]."</td>
                                      <td><button type='submit' name='Submit' class='btn btn-secondary'>Delete!</button></td>
                                    </form>
                                  </tr>";
                        }
                    }
                    echo "</tbody></table>";

                  } else {
                    echo "<h6>No Workouts to Remove!</h6>";
                  }

                  echo "<input type='hidden' name='workout' value=$this_rid>";
                ?>
             </div>
             <input name="workoutsArray[]" id="hiddenWorkoutInput" style="display: none">
             <input name="daysArray[]" id="hiddenDaysInput" style="display: none">
    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
        $("#addWorkoutButton").click(function(){
            workoutSelect = $("#workoutSelect");
			let days = $("#dayOfWeekChecks").find('input:checked');
			if (!days.length){ alert("Please select days of the week"); return;}
            workoutSelect.find(':selected').each(function(i){
              let daysString = "";
              days.each(function(i){
                  daysString += $(this).val();
              });
              $("#addedWorkoutTable").append("<tr><td style='display: none'>" + $(this).val() + "</td><td>" + $(this).text() + "</td><td>" + daysString + "</td></tr>");
            })
         });
         // Set workout variable on submit
          $("#addForm").submit(function(e){
            workoutsArray = []
            daysArray = []
            $("#addedWorkoutTable").find("tr").each(function(){
              workoutsArray.push($(this).children().eq(0).html());
              daysArray.push($(this).children().eq(2).html());
            });
            $("#hiddenWorkoutInput").val(workoutsArray.join());
            $("#hiddenDaysInput").val(daysArray.join());
          });
          $("#tableSearchInput").keyup(function(){
            searchTable()
          });
          $("#allOrUserSelect").change(function(){
            searchTable();
          });
      });
	
    </script>
  </body>
</html>
