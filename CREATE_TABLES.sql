use bjanczuk;

SET FOREIGN_KEY_CHECKS = 0;

# Create placeholder table so that @cmd is not null
CREATE TABLE IF NOT EXISTS Placeholder(id INT);
SELECT CONCAT('DROP TABLE ', GROUP_CONCAT(CONCAT(table_name)), ';') INTO @cmd FROM information_schema.tables WHERE table_schema = database();

PREPARE stmt FROM @cmd;
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE Users
(
username    VARCHAR(70) NOT NULL PRIMARY KEY,
email       VARCHAR(40) NOT NULL,
password    VARCHAR(255) NOT NULL,
name        VARCHAR(30),
age         INT,
weight      INT,
height      INT,
gender      VARCHAR(1),
date_joined DATE NOT NULL
);

CREATE TABLE WorkoutComments
(
 username    VARCHAR(70) NOT NULL,
 wid         INT NOT NULL,
 timestamp   date NOT NULL,
 text        VARCHAR(1000) NOT NULL
);

CREATE TABLE RoutineComments
(
 username    VARCHAR(70) NOT NULL,
 rid         INT NOT NULL,
 timestamp   date NOT NULL,
 text        VARCHAR(1000) NOT NULL
);

CREATE TABLE Exercises
(
 eid         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 creator     VARCHAR(70),
 name        VARCHAR(50),
 muscle_group VARCHAR(25),
 description VARCHAR(1500),
 FOREIGN KEY (creator) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE GeneralWorkouts
(
 wid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 creator VARCHAR(70),
 name VARCHAR(50),
 muscle_group char(25),
 description VARCHAR(1500),
 FOREIGN KEY (creator) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE SpecificWorkouts
(
 swid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 wid INT,
 username VARCHAR(70),
 date DATE NOT NULL,
 FOREIGN KEY (wid) REFERENCES GeneralWorkouts(wid),
 FOREIGN KEY (username) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE ExercisesInWorkouts
(
 eid INT,
 wid INT,
 FOREIGN KEY (eid) REFERENCES Exercises(eid) ON DELETE CASCADE,
 FOREIGN KEY (wid) REFERENCES GeneralWorkouts(wid) ON DELETE CASCADE
);

CREATE TABLE Sets
(
  sid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  creator VARCHAR(70),
  reps INT,
  weight INT,
  FOREIGN KEY (creator) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE SetsInSpecificWorkout
(
 sid INT,
 eid INT,
 swid INT,
 number INT,
 FOREIGN KEY (sid) REFERENCES Sets(sid) ON DELETE CASCADE,
 FOREIGN KEY (eid) REFERENCES Exercises(eid) ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY (swid) REFERENCES SpecificWorkouts(swid) ON DELETE CASCADE
);

CREATE TABLE Routines
(
 rid      INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(50),
 creator  VARCHAR(70),
 FOREIGN KEY (creator) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE DoRoutine
(
 username    VARCHAR(70),
 rid         INT,
 FOREIGN KEY (username) REFERENCES Users(username) ON DELETE SET NULL ON UPDATE CASCADE,
 FOREIGN KEY (rid) REFERENCES Routines(rid) ON DELETE CASCADE
);

CREATE TABLE WorkoutsInRoutines
(
 wid      INT,
 rid      INT,
 weekdays CHAR(7),
 FOREIGN KEY (wid) REFERENCES GeneralWorkouts(wid) ON DELETE CASCADE,
 FOREIGN KEY (rid) REFERENCES Routines(rid) ON DELETE CASCADE
);

CREATE TABLE ExerciseRatings
(
  username VARCHAR(70) NOT NULL,
  eid INT NOT NULL,
  rating_value FLOAT NOT NULL,
  count INT NOT NULL,
  FOREIGN KEY (username) REFERENCES Users(username) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (eid) REFERENCES Exercises(eid) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE WorkoutRatings
(
  username VARCHAR(70) NOT NULL,
  wid INT NOT NULL,
  rating_value FLOAT NOT NULL,
  count INT NOT NULL,
  FOREIGN KEY (username) REFERENCES Users(username) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (wid) REFERENCES GeneralWorkouts(wid) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE RoutineRatings
(
  username VARCHAR(70) NOT NULL,
  rid INT NOT NULL,
  rating_value FLOAT NOT NULL,
  count INT NOT NULL,
  FOREIGN KEY (username) REFERENCES Users(username) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (rid) REFERENCES Routines(rid) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER DATABASE bjanczuk CHARACTER SET utf8 COLLATE utf8_unicode_ci;
