<!DOCTYPE html>
<html>
  <head>
    <title>Workout</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class="col-2" style="padding: 5px">
          <a id="minButton" class="btn btn-secondary" href="pastWorkouts.php">back</a>
        </div>
      </div>
      <br>
	  <?php 
        $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
        mysqli_select_db($link, 'bjanczuk');

		if (isset($_POST['wid']) && isset($_POST['date']) && isset($_POST['data'])){
			$wid = $_POST['wid'];
			$data = json_decode($_POST['data'], true);
            if ($stmt = mysqli_prepare($link, "INSERT INTO SpecificWorkouts (username, wid, date) VALUES (?,?,?)")) {
                mysqli_stmt_bind_param($stmt, "sss", $_COOKIE["current_user"], $wid, $_POST["date"]);
                if (mysqli_stmt_execute($stmt)) {
					$swid = mysqli_insert_id($link);
            		if (($set_stmt = mysqli_prepare($link, "INSERT INTO Sets (reps, weight) VALUES (?,?)")) && ($join_stmt = mysqli_prepare($link, "INSERT INTO SetsInSpecificWorkout (sid, eid, swid, number) VALUES (?, ?, ?, ?)"))){
						foreach ($data as $eid => $sets){
							foreach ($sets as $number => $value){
            		    		mysqli_stmt_bind_param($set_stmt, "ss", $value['reps'], $value['weight']);
            		    		if (mysqli_stmt_execute($set_stmt)) {
									$sid= mysqli_insert_id($link);
            						mysqli_stmt_bind_param($join_stmt, "ssss", $sid, $eid, $swid, $number);
            						if (mysqli_stmt_execute($join_stmt)) {
										$_GET['wid'] = $wid;
										$_GET['swid'] = $swid;
									} else {
										echo mysqli_stmt_error($join_stmt);
									}
								} else {
									echo mysqli_stmt_error($set_stmt);
								}
							}
						}
					} else {
						echo "Error binding $set_stmt and $join_stmt";
					}
				} else {
					echo mysqli_stmt_error($stmt);
				}
			}
		} else if (count($_POST)){
			var_dump($_POST);
			echo "Error recording workout";
			exit(0);
		}

    	if (isset($_GET['swid']) && isset($_GET['wid'])){

		  $query = "SELECT gw.wid, gw.name, gw.muscle_group, gw.description, sw.date FROM GeneralWorkouts gw, SpecificWorkouts sw WHERE gw.wid = ".$_GET['wid']." AND sw.wid = gw.wid";
   	    	$result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
   	   		if ($result->num_rows < 1) {
   	   		    echo "Error";
   	   		} else {
   	    	     $tuple = mysqli_fetch_assoc($result);
                 echo "<br><h3>Workout: ".$tuple['name']."</h3>";

                 echo "<br><hr /><h5 style='display: inline'>Date: </h5><h6 style='display: inline'>" . $tuple['date'] . "</h6><br><br>";
                 echo "<h5 style='display: inline'>Muscle Group: </h5><h6 style='display: inline'>" . $tuple['muscle_group'] . "</h6><br><br>";

                 echo "<h5 style='display: inline'>Description: </h5><h6 style='display: inline'>" . $tuple['description'] . "</h6><br><br>";
   	    	 }
		  	$query = "SELECT sis.eid, e.name, s.reps, s.weight, sis.number FROM SetsInSpecificWorkout sis, Sets s, Exercises e WHERE e.eid = sis.eid AND sis.sid = s.sid AND sis.swid = ".$_GET['swid']." ORDER BY sis.eid, sis.number";
   	    	$result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
   	   		if ($result->num_rows < 1) {
   	   		    echo "Error";
   	   		} else {
				echo "<table class='table'><tbody>";
				echo "<thead class='thead-light'><th>Exercise</th><th>Sets (reps x weight)</th></thead>";
				$last_id = -1;
                while ($tuple = mysqli_fetch_assoc($result)){
					if ($tuple['eid'] != $last_id){
						if ($last_id >= 0){
							echo "</td></tr>";
						}
						echo "<tr><td>".$tuple['name']."</td><td>";
					}
					echo $tuple['reps']." x ".$tuple['weight']."<br>";
					$last_id = (int)$tuple['eid'];
				}
				echo "</tbody></table>";		
			}
		}
        mysqli_close($link);

      ?>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
      });
	
    </script>
  </body>
</html>
