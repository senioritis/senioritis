<!DOCTYPE html>
<html>
  <head>
    <title>Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="routines.php">Routines<span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
          </ul>
      </div>
    </nav>


    <div class="container">
      <br>
      <div class="row">
        <div class="col-10">
            <br><br>
           <h5>Successfully Removed Workout <?php 
            $this_rid = $_GET["routine_name"];
            $this_wid = $_GET["delworkout"];
            $this_day = $_GET["days"];

            $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
            mysqli_select_db($link, 'bjanczuk');

            $query = "SELECT name FROM GeneralWorkouts WHERE wid=".$this_wid."";
            $result = mysqli_query($link, $query) or die('Query failed '.mysqli_error($link));

            while ($tuple = mysqli_fetch_assoc($result)) {
                foreach ($tuple as $val) {
                    echo "$val";
                }
            }

            if ($stmt = mysqli_prepare($link, "DELETE FROM WorkoutsInRoutines WHERE wid=? AND rid=? AND weekdays=?")) {
               mysqli_stmt_bind_param($stmt, "iis", $this_wid, $this_rid, $this_day);
               mysqli_stmt_execute($stmt);
               mysqli_stmt_close($stmt);
            }
           ?>
           from Routine <?php
            $this_rid = $_GET["routine_name"];
            $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
            mysqli_select_db($link, 'bjanczuk');
            $query = "SELECT name FROM Routines where rid='"."$this_rid"."'";
            $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));

            while($tuple = mysqli_fetch_assoc($result)) {
                echo " ";
                foreach ($tuple as $col_key => $col_val) {
                    echo "$col_val";
                }
            }
  
           ?></h5>
           <br><br>
           <form action="list_workouts.php" method="get" id="form3">
            <?php
                $this_rid = $_GET["routine_name"];
                echo "<input type='hidden' name='workout' value='"."$this_rid"."'>";
            ?>
                <td><button type="submit" class="btn btn-secondary" id="back">Back</button></td>
           </form>

        </div>
      </div>
      <br> 
    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
      });
    
    </script>
  </body>
</html>
