
<!DOCTYPE html>
<html>
  <head>
    <title>Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script>
      // For the star rating buttons.
      $(function(){
            $('.rate_widget a').click(function(){
                // Make sure the chosen star stays selected.
                var star = $(this);
                console.log(star);
                star.closest('ul').find('.checked').removeClass('checked');
                star.addClass('checked');

                var wid = star.attr('id').split('_')[0];
                var star_value = star.attr('id').split('_')[1];
                var username = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1]
                jQuery.get('update_ratings_table.php', {'wid': parseInt(wid), 'star_value': parseInt(star_value), 'username': username, 'table': 'WorkoutRatings'}, function(d) {
                    console.log("Updated WorkoutRatings for wid " + wid);
                });
            });
        });
    </script>
    <script>
      // For the comment submission button.
      $(function(){
            $('#submitButton').click(function(){
                var form = $(this).parents('form:first');
                if ($.trim($('#comment_text').val()).length > 0) { // Only submit non-empty comments.
                    jQuery.get('submit_comment.php', {'id': form.attr('id'),
                            'table': 'RoutineComments',
                            'comment_text': $.trim($('#comment_text').val()),
                            'timestamp': new Date().toISOString().slice(0, 19).replace('T', ' ')}, function(d) {
                            if (d.length == 0) {
                                console.log("Added comment to RoutineComments for rid " + form.attr('id'));
                                $('#comment_text').val(""); // Reset the text box to be empty.
                                location.reload();
                            }
                    });
                }
            });
        });
    </script>
    <script>
      // For the comment deletion button.
      $(function(){
            $('.btn-secondary').click(function(){
                button = $(this);
                if (button.attr('id').includes("deleteButton_")) {
                    var cid = button.attr('id').split('_')[1];
                    jQuery.get('delete_comment.php', {'cid': cid, 'table': 'RoutineComments'}, function(d) {
                        if (d.length == 0) {
                            console.log("Deleted comment from RoutineComments with cid " + cid);
                            $('#commentRow_' + cid).hide();
                        }
                    });
                }
            });
        });
    </script>
    <style>
    .rate_widget ul{
        background: url('Images/star_empty.png') repeat-x;
    }

    .rate_widget a:hover{
        background: url('Images/star_highlight.png') repeat-x;
    }

      .rate_widget a:active,
      .rate_widget a:focus,
      .rate_widget a.checked{
          background: url('Images/star_full.png') repeat-x;
      }
    .rate_widget ul{
        position:relative;
        width:160px; /*5 times the width of your star*/
        height:32px; /*height of your star*/
        overflow:hidden;
        list-style:none;
        margin:0;
        padding:0;
        background-position: left top;
    }

    .rate_widget li{
        display: inline;
    }

    .rate_widget a{
        position:absolute;
        top:0;
        left:0;
        text-indent:-1000em;
        height:32px; /*height of your star*/
        line-height:32px; /*height of your star*/
        outline:none;
        overflow:hidden;
        border: none;
    }

    .rate_widget a.one-star{
        width:20%;
        z-index:6;
    }

    .rate_widget a.two-stars{
        width:40%;
        z-index:5;
    }

    .rate_widget a.three-stars{
        width:60%;
        z-index:4;
    }

    .rate_widget a.four-stars{
        width:80%;
        z-index:3;
    }

    .rate_widget a.five-stars{
        width:100%;
        z-index:2;
    }

    .rate_widget a.current{
        z-index:1;
    }
    </style>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class ="col-10">
        <a id="backButton" class="btn btn-secondary" href="routines.php">back</a>
        </div>
      </div>
      <div class="row">
        <div class="col-10">
            <?php 
            $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
            mysqli_select_db($link, 'bjanczuk');
            $this_rid=$_GET["workout"];
            if (isset($_GET["workoutsArray"]) && isset($_GET["daysArray"])) {
              if ($this_rid &&  is_array($_GET["workoutsArray"]) && is_array($_GET["daysArray"])) {
                $workouts = $_GET["workoutsArray"];
                $days = $_GET["daysArray"];
                foreach ($workouts as $key => $this_wid) {
                    if ($stmt = mysqli_prepare($link, "INSERT INTO WorkoutsInRoutines (wid, rid, weekdays) VALUES (?,?,?)")) {
                        mysqli_stmt_bind_param($stmt, "iis", $this_wid, $this_rid, $days[$key]);
                        if (mysqli_stmt_execute($stmt)) {
                            
                        } else {
                            echo "Failed to add workout: ".mysqli_stmt_error($stmt);
                        }
                        mysqli_stmt_close($stmt);
                    }
                }
              }
            }

            $query = "SELECT name FROM Routines where rid='"."$this_rid"."'";
            $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));

            while($tuple = mysqli_fetch_assoc($result)) {
                echo "<br><br><h3>Routine: ";
                foreach ($tuple as $col_key => $col_val) {
                    echo "$col_val";
                }
                echo "</h3>";
            }

            $query = "SELECT * FROM Routines, RoutineRatings where Routines.rid='"."$this_rid"."' AND Routines.rid = RoutineRatings.rid";
            $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));

            while($tuple = mysqli_fetch_assoc($result)) {
                echo "<br><hr /><h5 style='display:inline'>Creator: </h5><h6 style='display:inline'>" . $tuple['creator'] . "</h6><br><br>";

                if ($tuple["count"] == "0") { echo "<h5 style='display: inline'>Current Rating: </h5><h6 style='display: inline'>No Rating</h6><br><br>"; }
                else { echo "<h5 style='display: inline'>Current Rating: </h5><h6 style='display: inline'>" . number_format(floatval($tuple["rating_value"]), 2) . " / 5.00</h6><br><br>"; }

                // Display 5 stars.
                // Taken from https://jsfiddle.net/BHaTu/
                echo "<h5 style='display: inline; float: left'>Rate It: </h5>";
               echo "
               <div class='rate_widget' style='display: inline; float: left; margin-left: 10px'>
                  <ul>
                    <li><a href='#' id='" . $tuple["rid"] . "_1' class='one-star'>1</a></li>
                    <li><a href='#' id='" . $tuple["rid"] . "_2' class='two-stars'>2</a></li>
                    <li><a href='#' id='" . $tuple["rid"] . "_3' class='three-stars'>3</a></li>
                    <li><a href='#' id='" . $tuple["rid"] . "_4' class='four-stars'>4</a></li>
                    <li><a href='#' id='" . $tuple["rid"] . "_5' class='five-stars'>5</a></li>
                  </ul>
               </div><br><hr />"; 
            }

            ?>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-10">
            <h3>Workouts in this Routine:</h3>
        </div>
      </div>
      <br>
      <div class="row" id="tableDiv">
      <?php
        $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
        mysqli_select_db($link, 'bjanczuk');
        $this_rid=$_GET["workout"];
        $query1 = "SELECT wid FROM WorkoutsInRoutines where rid='"."$this_rid"."'";
        $result1 = mysqli_query($link, $query1) or die('Query failed '. mysqli_error($link));
               
        if ($result1->num_rows < 1) {
            echo "<div class='container'>No workouts in this routine!</div><br><br><br>";
        } else {
            echo "<table class='table' id='workoutTable'>\n";
            echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>Workout Name</th>\n\t\t<th>Muscle Group</th>\n\t\t<th>Description</th>\n\t</thead>\n\t<tbody>\n";
            $wids=array();
            while ($tuple1 = mysqli_fetch_assoc($result1)) {
                foreach ($tuple1 as $col_key1 => $col_val1) {
                    array_push($wids, $col_val1);
                    $query2 = "SELECT name, muscle_group, description FROM GeneralWorkouts WHERE wid='"."$col_val1"."'";
                
                    $result2 = mysqli_query($link, $query2) or die('Query failed '. mysqli_error($link));
                    while ($tuple2 = mysqli_fetch_assoc($result2)) {
                        echo "\t<tr>\n";
                        foreach ($tuple2 as $col_key2 => $col_val2) {
                            echo "\t\t<td class=$col_key2> $col_val2 </td>\n";
                        }
                        echo "\t</tr>\n";
                    }
                }
            }
            echo "\t</tbody>\n</table>\n";  

        }

        $this_rid=$_GET["workout"];
        echo "</div>";
        $inside = 0;
        $query3 = "SELECT rid FROM Routines WHERE creator='".$_COOKIE['current_user']."'";
        $result3 = mysqli_query($link, $query3) or die('Query failed'.mysqli_error($link));
        while ($tuple = mysqli_fetch_assoc($result3)) {
            foreach ($tuple as $col_val) {
                if ($this_rid == $col_val)
                    $inside = 1;
            }
        }
        if ($inside == 1) {
            echo "<div class='row'>
                <div class='colsm-5'>
                    <form action='r_w_form.php' method='get' id='form1'>
                        <input type='hidden' name='addworkout' value='"."$this_rid"."'>
                        <td><button type='submit' class='btn btn-secondary' id='addexercise'>add Workouts</button></td>
                    </form>
                </div>
                <div class='col-sm-5'>
                    <form action='r_w_remove_form.php' method='get' id='form2'>
                        <input type='hidden' name='rmworkout' value='"."$this_rid"."'>
                        <td><button type='submit' class='btn btn-secondary' id='rmexercise'>remove Workouts</button></td>
                    </form>
                </div>
				</div>
				<br/><br/>";
        }
        ?>

        <div class="row">
        <div class="col-10">
            <h3>Comments:</h3>
        </div>
      </div>
      <br>
      <div class="row" id="tableDiv">
      <?php

        $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
        mysqli_select_db($link, 'bjanczuk');
        $this_rid=$_GET["workout"];
        $query = "SELECT cid, username, date(timestamp) as date, text FROM RoutineComments where rid='"."$this_rid"."' ORDER BY timestamp DESC";
        $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
               
        if ($result->num_rows < 1) {
            echo "<h6>There are no comments about this routine yet! Be the first one to say something:</h6><br><br>";
        } else {
            echo "<table class='table' id='commentsTable'>\n";
            echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>User</th>\n\t\t<th>Date</th>\n\t\t<th>Comment</th>\n\t\t<th></th>\n\t</thead>\n\t<tbody>\n";
            while ($tuple = mysqli_fetch_assoc($result)) {
                echo "<tr id='commentRow_" . $tuple['cid'] . "'>";
                echo "\t\t<td class=username> " . $tuple['username'] . "</td>\n";
                echo "\t\t<td class=date> " . $tuple['date'] . "</td>\n";
                echo "\t\t<td class=text> " . $tuple['text'] . "</td>\n";

                if ($tuple['username'] == $_COOKIE['current_user']) {
                    echo "\t\t<td><button type=button id=\"deleteButton_" . $tuple['cid'] . "\" class=\"btn btn-secondary\">delete</button></td>\n";
                }
                else { echo "\t\t<td > </td>\n"; }
                echo "</tr>";
            }
            echo "\t</tbody>\n</table>\n";  

        }
        echo "</div>";

        echo "<form id='" . $_GET["workout"] . "' method='get'>
                <P>
                <TEXTAREA name='comment_text' id='comment_text' rows='4' cols='150' placeholder='Enter comment here (maximum of 1000 characters)...'></TEXTAREA>
                <div class='container'><div class='row'><div class ='col-10'>
                    <button id='submitButton' type=button class='btn btn-secondary'>Submit</button>
                </div></div>
                </P>
              </form>";

      ?>      
    </div>
    <br><br>

    <script>
      $(document).ready(function(){
        $("#addButton").click(function(){
          $("#addForm").toggle();
        });
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
	});
	
    </script>
  </body>
</html>
