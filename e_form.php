<!DOCTYPE html>
<html>
  <head>
    <title>Exercises</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="exercises.php">Exercises <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>

    <br>
    <div class="container">
      <div class="row">
        <div class="col-10">
          <h3>Add Exercises</h3>
        </div>
        <div class="col-2" style="padding: 5px">
          <a id="minButton" class="btn btn-secondary" href="exercises.php">Back</a>
        </div>
      </div>
      <br>
        <form method="post" action="exercises.php" id="addForm">
          <h5>Exercise Name:</h5>
          <input id="exerciseName" type="text" name="name"/><br><br>
          <h5>Muscle Group:</h5>
          <select name="muscle_dropdown">
                <option value="Abs">Abs</option>
                <option value="Arms">Arms</option>
                <option value="Back">Back</option>
                <option value="Chest">Chest</option>
                <option value="Full_Body">Full Body</option>
                <option value="Miscellaneous">Miscellaneous</option>
                <option value="Shoulders">Shoulders</option>
        </select><br><br>
          <h5>Description:</h5>
          <input id="exerciseDescription" type="text" name="description"/><br>
          <br>
          <input type="submit" name="Submit" class="btn btn-success"/>
        </form>

    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
		$("#addForm").submit(function(e){
			if ($("#exerciseName").val().length == 0){
				alert('Please enter an exercise name');
			} else if ($("#exerciseDescription").val().length == 0){
				alert("Please enter an exercise description");
			} else {
				return true;
			}
			return false;
		});
      });
    </script>
  </body>
</html>
