<!DOCTYPE html>
<html>
  <head>
    <title>Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<style>
		.modalCheckLabel {
			padding: 10px;
		}
	</style>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="routines.php">Routines<span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class="col-10">
           <h3>Add Routines</h3>
        </div>
        <div class="col-2" style="padding: 5px">
          <a id="minButton" class="btn btn-secondary" href="routines.php">Back</a>
        </div>
      </div>
      <br> 
        <form method="get" action="routines.php" id="addForm">
            <div class="form-row">
                <div class="form-group col-sm-4">
                    <label for="addRoutineName">Routine Name:</label>
                    <input class="form-control" id="addRoutineName" type="text" name="name"/><br><br>
                </div>
            </div>
            <h5>Workouts:</h5><br>
			<p> Select your workouts from the list on the left, select the weekdays to do the workouts, and click add to add them to your routine.</p>
            <div class="row">
              <div class="col-sm-6 form-group">
                  <?php
                    $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or die ('Database connection error');
                    mysqli_select_db($link, 'bjanczuk');

                    $query = "SELECT wid, name FROM GeneralWorkouts";
                    $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
                    if ($result->num_rows < 1) {
                        echo "<p>No Workouts found.  Create one in the workouts tab</p>";
                    } else {
		        		echo "<table class='table' id='workoutTable'><tbody>\n";
			  			echo "<thead class='thead-light'><tr><th colspan=2>Workouts</th></tr></thead>";
                        while ($tuple = mysqli_fetch_assoc($result)){
                          echo "<tr><td class='workoutName'>".$tuple['name']."</td><td class='wid' style='display: none'>".$tuple['wid']."</td><td><button type=button class='btn btn-success addWorkoutButton'>Add</button></td</tr>\n";
                        }
                        echo '</tbody></table>';
                      }


                  ?>
              </div>
			<!-- Modal -->
			<div class="modal fade" id="dayModal" tabindex="-1" role="dialog" aria-labelledby="dayModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="dayModalLabel"></h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
					<div id="modalWid" style="display: none"></div>
					<div id="modalWorkoutName" style="display: none"></div>
					<div id="modalDayChecks">
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="U">Sun</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="M">Mon</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="T">Tue</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="W">Wed</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="R">Thu</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="F">Fri</label>
                    	<label class="checkbox-inline modalCheckLabel"><input type="checkbox" value="S">Sat</label>
					</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" id="modalClose" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary" id="modalAdd">Add Workout</button>
			      </div>
			    </div>
			  </div>
			</div>
              <input name="workoutArray" id="hiddenWorkoutInput" style="display: none">
              <input name="daysArray" id="hiddenDaysInput" style="display: none">
              <div class="form-group col-sm-6">
				<h6>Workouts in Routine:</h6>
               <table class='table table-bordered'>
                 <tbody id="addedWorkoutTable">
					<thead class='thead-light'><tr><th>Workout</th><th>Days of the Week</th></thead>
                 </tbody>
               </table>
              </div>
              </div>
            <div class="form-row">
                <input type="submit" name="Submit" class="btn btn-success"/>
            </div>
        </form>
    </div><br><br><br>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
		$(".addWorkoutButton").click(function(e){
			$("#dayModal").modal("show");
			let workoutName = $(this).parents().find(".workoutName").html();
			let wid = $(this).parents().find(".wid").html();
			$("#dayModalLabel").html("Select Days for '" + workoutName + "'");
			console.log(workoutName);
			$("#modalWorkoutName").html(workoutName);
			$("#modalWid").html(wid);
		});
		$("#modalAdd").click(function(e){
			let workoutName = $("#modalWorkoutName").html();
			let wid = $("#modalWid").html();
			let dayString = ''
			$("#modalDayChecks").find("input:checked").each(function(e){
				dayString += $(this).val();
			});
			$("#dayModal").modal("hide");
			$("#modalDayChecks").find("input:checked").each(function(e){
				$(this).prop('checked', false);
			});
			if (dayString.length){
              $("#addedWorkoutTable").append("<tr><td style='display: none'>" + wid + "</td><td>" + workoutName + "</td><td>" + dayString + "</td></tr>");

			}

		});
// Set workout variable on submit
          $("#addForm").submit(function(e){
			if ($("#addRoutineName").val().length == 0){
				alert("Please enter a routine name");
				return false;
			}

            workoutsArray = []
            daysArray = []
            $("#addedWorkoutTable").find("tr").each(function(){
              workoutsArray.push($(this).children().eq(0).html());
              daysArray.push($(this).children().eq(2).html());
            });
            $("#hiddenWorkoutInput").val(workoutsArray.join());
            $("#hiddenDaysInput").val(daysArray.join());
          });
      });
	
    </script>
  </body>
</html>
