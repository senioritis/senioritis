<!DOCTYPE html>
<html>
  <head>
    <title>Senioritis Home Page</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <?php if (isset($_COOKIE['current_user'])) : ?>
            <li class='nav-item'>
              <a class='nav-link' href='exercises.php'>Exercises</a>
            </li>
            <li class='nav-item'>
              <a class='nav-link' href='workouts.php'>Workouts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
            </li>
            <li class='nav-item'>
              <a class='nav-link' href='routines.php'>Routines</a>
            </li>
          </ul>
  		  <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout' href=''>Logout</a>
              </li>
  	      </ul>
		  <?php else : ?>
		  </ul>
		  <ul class="navbar-nav ml-auto">
            <li class='nav-item'>
              <a class='nav-link' href='login.php'>Login</a>
            </li>
	      </ul>
          <?php endif; ?>
      </div>
    </nav>
    	<br><br>
	<h1 style="text-align: center; color: red">Welcome to the Workout Planner.</h1>

	<br><br>
	<img src="Images/workout_art.jpg" style="display: block; margin-left: auto; margin-right: auto" height="40%" width="40%"/>

	<br>
	<h5 style="text-align: center">Here, you'll find everything you need related to working out.<br>
		Search around to get ideas for exercises or workouts.<br>Then, schedule your own weekly 
		routines and start getting chiseled. Log in above to get started!</h5>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script>
  $(document).ready(function(){
    $("#logout").click(function(){
      document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
      location.reload();
    });
  });
  </script>
  </body>
</html>
