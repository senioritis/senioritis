# workout db etl script
import requests
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import sqlalchemy

# get create muscle group dictionary (arms, back, legs)
muscle_group_dict = {}
r = requests.get('https://wger.de/api/v2/exercisecategory/')
r_json = r.json()
for muscle_group in r_json['results']:
    muscle_group_dict[muscle_group['id']] = muscle_group['name']

# get create equipment dictionary (barbell, dumbell)
equipment_dict = {}
r = requests.get('https://wger.de/api/v2/equipment/')
r_json = r.json()
for equip in r_json['results']:
    equipment_dict[equip['id']] = equip['name']

# get exercises in english
#language 2 is english, status 2 ensures the exercise is approved
payload = {'limit': '1000', 'language' : '2', 'status': '2'}
r = requests.get('https://wger.de/api/v2/exercise/', params=payload)
r_json = r.json()
df = pd.DataFrame.from_records(r_json['results'])
df['muscle_group'] = df.apply(lambda row: muscle_group_dict[row['category']],axis=1)
df['creator'] = "rj"
df['description'] = df['description'].str.encode('utf-8', errors='ignore')
# final_df = df[['id', 'uuid', 'name', 'rating', 'muscle_group', 'description', 'license_author']]

exercise_df = df[['name', 'muscle_group', 'description', 'creator']]
# connect to db and push up df
USR = "bjanczuk"
PWD = "bartosz"
DB_ADDR = "localhost"
DB = "bjanczuk"
connnection_string = 'mysql://{}:{}@{}/{}'.format(USR, PWD, DB_ADDR, DB)
engine = create_engine(connnection_string)
con = engine.connect()
exercise_df.to_sql(con=con, name='Exercises', if_exists='append', index=False,
                    dtype={'name': sqlalchemy.types.VARCHAR(length=50),
                           'muscle_group': sqlalchemy.types.VARCHAR(length=25),
                           'description': sqlalchemy.types.VARCHAR(length=1500),
                           'creator': sqlalchemy.types.VARCHAR(length=70)})
