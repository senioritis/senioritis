<html>
  <head>
    <meta charset="UTF-8">
    <title>Workout Planner</title>
  </head>
  <body>
	Workout Planner - Project Details
	<br><br>
	1) The goal of this web app will be to help people design and plan workouts. Users will be able to create personal accounts which store their workout history, but the app will
		also be functional for unauthenticated users. In general, there will be a large selection of specific workouts to choose from (and users will have the option to enter
		their own as well), and there will also be a selection of workout combinations to choose from (again, users will be able to create their own too). Details about the
		workout type, intensity, time commitment, necessary equipment, etc. will be available. Users will be able to rate workouts and workout routines as well, and
		suggestions for future workouts will be provided as well. Additionally, there will be an option to intertwine a user account with a FitBit account. Finally, there will
		be a discussion board, where users will be able to post comments, suggestions, questions, and so on.
	<br>
	<br>
	2) This application will be useful firstly because of the sheer number of people who regularly workout, many of whom could benefit from getting suggestions and ideas from an
		app like this. It will also be useful because of the user account functionality, which will allow for use of user input, the ratings system, suggestions, and the commend board.
		Sites containing workout routine information do exist, but none exist that combine this much functionality into one app.
	<br>
	<br>
	3) The initial (and majority of the) data will come from workout datasets found online and existing workout websites. The data will obviously need to be cleaned and refined in
		order to fit our models, but there will certainly be enough there to make the website usable right off the bat. In addition to this, users will be able to input some of
		their own routines, which will also get added to the data mentioned above.
	<br>
	<br>
	4) As far as basic functions go, we should have everything one would expect. There'll be an ability for users to query workouts, workout routines, and comments by any number of
		attributes. They'll be able to insert both by adding their own ideas to the dataset and by saving personal combinations of pre-existing workouts and routines. Deletion
		and updating will be available by just deleting or editing either of those two aforementioned insertions. Of course, as ratings and upvotes come in, the corresponding
		entries in data tables will be updated as well.
	<br>
	&nbsp;&nbsp;&nbsp;&nbsp;As far as advanced functions go, we'll have two primary ones. The first will be a workout and workout routine recommendation system, based on the ratings
		users give and the general workouts/routines they use. The second will be integration with FitBit, allowing users to couple their workout planning with their ongoing
		activity tracking.

	<br>
	<br>
	<a href="https://drive.google.com/file/d/0B8IBGRxd5_BWSE5wZk5IanZZVnRpTHZLendlNURPOXhGYUZJ/view?usp=sharing">ER Diagram</a>
	<br>
	<br> Team <a href="plan.php">Development Plan</a>
        <br>
        <br> Team <a href="../index.php">Demo</a>
  </body>
</html>
