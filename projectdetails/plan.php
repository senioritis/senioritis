<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Development Plan</title>
  </head>
  <body>
	1.
	<br>Users(<u>username</u>, email, password, name, age, weight, gender, dateJoined)
	<br>Comments(<u>uid</u>, routineId, timestamp, text)
	<br>Exercises(<u>eid</u>, name, creator, muscleGroup, description)
	<br>GeneralWorkouts(<u>wid</u>, name, creator, muscleGroup)
	<br>SpecficWorkouts(<u>username</u>, <u>rid</u>, day)
	<br>ExercisesInWorkout(<u>name</u>, <u>wid</u>)
	<br>Sets(<u>sid</u>, creator, reps, weight)
	<br>SetsOfExercises(<u>eid</u>, <u>swid</u>)
	<br>Routines(<u>rid</u>, weekdays, creator)
	<br>DoRoutine(<u>username</u>, <u>rid</u>)
	<br>WorkoutInRoutine(<u>wid</u>, <u>rid</u>)
	<br>WorkoutRatings(<u>uid</u>, <u>wid</u>, rating)
	<br>RoutineRatings(<u>username</u>, <u>rid</u>, rating)
	<br>ExerciseRatings(<u>uid</u>, <u>eid</u>, rating)
	<br>
	<br>
	2.
	<br> Our database choice will be standard; we'll just be using MySQL.
	<br>
	<br>
	3.
	<br> Data for this project will be accumulated in a couple of ways. Firstly, we will use the api at <a href="https://wger.de/en/software/api" target='_blank'>https://wger.de/en/software/api</a> to compile preset exercieses and add them to our database
		so that the app has data ready for initial users. Secondly, users themselves will be able to generate data themselves by creating workouts and routines, as well as by
		rating things.
	<br>
	<br>
	4.
	<br> Bart Janczuk will be tasked with creating and maintaining the SQL database, RJ Stempak will deal with security and user input, Albert Oh and Joe Kimlinger will work on front-end/presentation,
		and all four will work on SQL querying and the advanced functions.
	<br>
	<br>
	5.
	<br>  October 8: Database created and ready for data input (i.e. it can be updated/added/deleted)
	<br>  October 15: Initial online data has been downloaded and inserted into the database
	<br>  October 22: Front-end is presentable and usable
	<br>&nbsp;&nbsp;&nbsp;&nbsp; - Home page, profile page + login, search bar, exercise pages
	<br>  November 19: First advanced function is finished
	<br>  December 3: Second advanced function is finished, front-end is totally finished
	<br>  December X (Demo Day): Video is filmed and edited, report is written
  </body>
</html>
