<!DOCTYPE html>
<html>
  <head>
    <title>Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="workouts.php">Workouts <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class='col-10'>
         <form action="list_exercises.php" method="get" id="form2">
            <?php
                $this_wid=$_GET["rmexercise"];
                echo "<input type='hidden' name='exercise' value='"."$this_wid"."'>";
            ?>
                <td><button type="submit" class="btn btn-secondary" id="back">Back</button></td>        
        </div>
      </div>
      <br>

      <div class="row">
        <div class="col-10">
           <h3>Remove Exercises from<?php
           $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
           mysqli_select_db($link, 'bjanczuk');
           $this_wid=$_GET["rmexercise"];
           $query = "SELECT name FROM GeneralWorkouts where wid='"."$this_wid"."'";
           $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));

           while($tuple = mysqli_fetch_assoc($result)) {
                echo " ";
                foreach ($tuple as $col_key => $col_val) {
                    echo "$col_val";
                }
            }

           ?></h3>
        </div>
      </div>
      <br>
        <form method="get" action="list_exercises.php" id="addForm">
          <h4>Exercises to Remove:</h4>
          (Hold down the Ctrl (windows) / Command (Mac) button to select multiple options. Hold Shift or click and drag with mouse to select multiple together.)<br><br>
          <select name="exercises[]" multiple>
            <?php
                $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or die ('Database connection error');
                mysqli_select_db($link, 'bjanczuk');
                $this_wid = $_GET["rmexercise"];
                $query1 = "SELECT eid FROM ExercisesInWorkouts WHERE wid='"."$this_wid"."'";
                $result1 = mysqli_query($link, $query1) or die('Query failed '.mysqli_error($link));
                $excluded = array();
                if ($result1->num_rows>0) {
                    while ($tuple = mysqli_fetch_assoc($result1)) {
                        foreach($tuple as $col_val) {
                            array_push($excluded, $col_val);
                        }
                    }
                }

                $excluded_new=implode(",", $excluded);
		        $query = "SELECT eid, name FROM Exercises WHERE creator = '".$_COOKIE['current_user']."' AND eid IN ($excluded_new)";

                //$query = "SELECT eid, name FROM Exercises WHERE creator = '".$_COOKIE['current_user']."' AND muscle_group = '".$_POST["muscle_dropdown"]."'";
                $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
	  	        if ($result->num_rows < 1) {
	  		        echo "<br>No Exercises found in this workout.<br>";
	  	        } else {
                    while ($tuple = mysqli_fetch_assoc($result)){
                        $counter = 1;
                        $temp = NULL;
                        foreach($tuple as $col_val) {
                            if ($counter % 2 == 0) {
                                echo "<option value='"."$temp"."'>"."$col_val"."</option>\n";
                            } else {
                                $temp = $col_val;
                            }
                            $counter++;
                        }
                    }
                }

          echo "</select>";
          echo "<input type='hidden' name='removed' value='dummy'>";
          echo "<input type='hidden' name='exercise' value='"."$this_wid"."'>";
          ?>
          <br><br>
          <input type="submit" name="Submit" class="btn btn-success"/>
        </form>
    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
      });
	
    </script>
  </body>
</html>
