<!DOCTYPE html>
<html>
  <head>
    <title>Past Workouts</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>


	<div class="container">
      <br>
      <div class="row">
        <div class="col-12">
           <h3>Past Workouts</h3>
        </div>
		<div class="col-sm-10">
		   <p>On this page, you can record your past workouts including the number of sets, reps, and weight of each exercise.  Use this to see your progress over time and what you should be shooting for on your next workout.</p>
		</div>

        <div class="col-2" style="padding: 5px">
          <button  type=button id="addButton" class="btn btn-secondary">Record a Workout</a>
        </div>
      </div>
      <br>
      <div class="row" id="tableDiv">
        <?php
          $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or die ('Database connection error');
          mysqli_select_db($link, 'bjanczuk');

		  $query = "SELECT sw.wid, sw.swid, gw.name, gw.muscle_group, sw.date from SpecificWorkouts sw, GeneralWorkouts gw WHERE sw.username = '".$_COOKIE['current_user']."' AND sw.wid = gw.wid ORDER BY sw.date DESC";
          $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
	  	  if ($result->num_rows < 1){
	  		echo "<h5>You have not yet recorded a workout.  You can do so by selecting \"Record a Workout\".</h5><br>";
	  	  } else {
			echo "<div class='form-group col-sm-12'><input class='form-control' type='text' id='tableSearchInput' placeholder='Search Past Workouts'></div>";
			echo "<table class='table' id='pastWorkoutsTable'>\n";
            echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>Date</th>\n\t\t<th>Name</th>\n\t\t<th>Muscle Group</th>\n\t\t<th>Exercises</th>\t</tr>\n\t</thead>\n\t<tbody>\n";
			echo "<tr id='noMatchToSearch' style='display: none'><td style='text-align: center' colspan=5 ><h6>No workouts match search</h6></td></tr>";
            while ($tuple = mysqli_fetch_assoc($result)){
              echo "\t<tr>\n";

              echo "\t\t<td class=date> " . $tuple["date"] . " </td>\n";
              echo "\t\t<td class=name> " . $tuple["name"] . " </td>\n";
              echo "\t\t<td class=muscle_group> " . $tuple["muscle_group"] . " </td>\n";


              $curr_id = (int)$tuple["wid"];
              echo "<form action='p_form.php' method='get' id='expandForm'>
                        <input type='hidden' name='wid' value='".(int)$tuple['wid']."'>
                        <input type='hidden' name='swid' value='".(int)$tuple['swid']."'>
                        <td><button type='submit' class='btn btn-secondary' id='expandExercise"."$curr_id"."'>Expand</button></td>
                    </form>";

              echo "\t</tr>\n";
            }
              echo "\t</tbody>\n</table>\n";
          }

          mysqli_free_result($result);
		  echo "
			<!-- Modal -->
			<div class='modal fade' id='workoutModal' tabindex='-1' role='dialog' aria-labelledby='workoutModalLabel' aria-hidden='true'>
			  <div class='modal-dialog' role='document'>
			    <div class='modal-content'>
			      <div class='modal-header'>
			        <h5 class='modal-title' id='workoutModal'>Select a Workout</h5>
			        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
			          <span aria-hidden='true'>&times;</span>
			        </button>
			      </div>
			      <div class='modal-body'>";

          $query = "SELECT gw.wid, gw.creator, gw.name, gw.muscle_group FROM GeneralWorkouts gw";
          $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
	  	  if ($result->num_rows < 1){
	  		echo "<h5>No Workouts found.</h5>";
	  	  } else {
			echo "<div class='form-group col-sm-12'><input class='form-control' type='text' id='workoutTableSearchInput' placeholder='Search Workouts'></div>";
			echo "<div class='form-group col-sm-12'><select id='allOrUserSelect' class='form-control'><option>All workouts</option><option>My workouts</option></select></div>";
			echo "<table class='table' id='workoutsTable'>\n";
            echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>Name</th>\n\t\t<th>Creator</th>\n\t\t<th>Muscle Group</th>\n\t\t<th>Exercises</th>\t</tr>\n\t</thead>\n\t<tbody>\n";
			echo "<tr id='workoutsNoMatchToSearch' style='display: none'><td style='text-align: center' colspan=5 ><h6>No workouts match search</h6></td></tr>";
            while ($tuple = mysqli_fetch_assoc($result)){
              echo "\t<tr>\n";

              echo "\t\t<td class=name> " . $tuple["name"] . " </td>\n";
              echo "\t\t<td class=creator> " . $tuple["creator"] . " </td>\n";
              echo "\t\t<td class=muscle_group> " . $tuple["muscle_group"] . " </td>\n";

              $curr_id = (int)$tuple["wid"];
              echo "<form action='p_add_form.php' method='get' id='addForm'>
                        <input type='hidden' name='exercise' value='".$curr_id."'>
                        <td><button type='submit' class='btn btn-success' id='expandExercise".$curr_id."'>Record</button></td>
                    </form>";

              echo "\t</tr>\n";
            }
              echo "\t</tbody>\n</table>\n";
          }

          mysqli_free_result($result);
          mysqli_close($link);


		  echo "</div>
			      <div class='modal-footer'>
			        <button type='button' class='btn btn-secondary' id='modalClose' data-dismiss='modal'>Close</button>
			        <button type='button' class='btn btn-primary' id='modalAdd'>Record Workout</button>
			      </div>
			    </div>
			  </div>
			</div>";
        ?>
      </div>
    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
		$("#tableSearchInput").keyup(function(){
			searchTable("#pastWorkoutsTable", "#tableSearchInput", "#noMatchToSearch", false);
		});
		$("#workoutTableSearchInput").keyup(function(){
			searchTable("#workoutsTable", "#workoutTableSearchInput", "#workoutsNoMatchToSearch", true);
		});
		$("#allOrUserSelect").change(function(){
			searchTable("#workoutsTable", "#workoutTableSearchInput", "#workoutsNoMatchToSearch", true);
		});
		$("#addButton").click(function(e){
			$("#workoutModal").modal("show");
		});
      });

	  function userSearch(){
	  }

	  function searchTable(tableName, searchInput, noMatch, filterUser) {
          let filter = $(searchInput).val().toLowerCase();
          let current_user = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1];
          let userFilter = ($("#allOrUserSelect").val() == "My workouts") ? current_user : "";
		  let numShown = 0;
          $(tableName).find("tr").each(
            function(i){
                creator = $(this).find("td.creator").html();
                if (creator && filterUser){
                    if (creator.toLowerCase().indexOf(userFilter) < 0){
                        $(this).hide();
                        return true;
                    }
				} else if ($(this).attr('id') && "#" + $(this).attr('id') == noMatch){
					return true;
				}
                $(this).find("td").each(
                    function(){
                        if ($(this).html().toLowerCase().indexOf(filter) > -1){
                            $(this).parent().show()
			    			numShown++;
                            return false;
                        } else {
                            $(this).parent().hide()
                        }
                    }
            	);
			if (!numShown){
				$(noMatch).show();
			} else {
				$(noMatch).hide();
			}
            });
		}
    </script>
  </body>
</html>
