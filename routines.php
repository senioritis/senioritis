<!DOCTYPE html>
<html>
  <head>
    <title>Routines</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script>
      $(function(){
            $('.rate_widget a').click(function(){
                // Make sure the chosen star stays selected.
                var star = $(this);
                console.log(star);
                star.closest('ul').find('.checked').removeClass('checked');
                star.addClass('checked');

                var rid = star.attr('id').split('_')[0];
                var star_value = star.attr('id').split('_')[1];
                var username = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1]
                jQuery.get('update_ratings_table.php', {'rid': parseInt(rid), 'star_value': parseInt(star_value), 'username': username, 'table': 'RoutineRatings'}, function(d) {
                    console.log("Updated RoutineRatings for rid " + rid);
                });
            });
        });
    </script>
	<style>
		.creator {
			display: none;
		}
     .rate_widget ul{
        background: url('Images/star_empty.png') repeat-x;
    }

    .rate_widget a:hover{
        background: url('Images/star_highlight.png') repeat-x;
    }

      .rate_widget a:active,
      .rate_widget a:focus,
      .rate_widget a.checked{
          background: url('Images/star_full.png') repeat-x;
      }
    .rate_widget ul{
        position:relative;
        width:160px; /*5 times the width of your star*/
        height:32px; /*height of your star*/
        overflow:hidden;
        list-style:none;
        margin:0;
        padding:0;
        background-position: left top;
    }

    .rate_widget li{
        display: inline;
    }

    .rate_widget a{
        position:absolute;
        top:0;
        left:0;
        text-indent:-1000em;
        height:32px; /*height of your star*/
        line-height:32px; /*height of your star*/
        outline:none;
        overflow:hidden;
        border: none;
    }

    .rate_widget a.one-star{
        width:20%;
        z-index:6;
    }

    .rate_widget a.two-stars{
        width:40%;
        z-index:5;
    }

    .rate_widget a.three-stars{
        width:60%;
        z-index:4;
    }

    .rate_widget a.four-stars{
        width:80%;
        z-index:3;
    }

    .rate_widget a.five-stars{
        width:100%;
        z-index:2;
    }

    .rate_widget a.current{
        z-index:1;
    }
	</style>

  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="routines.php">Routines <span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>

    <div class="container">
        <br>
        <div class="row">
          <div class="col-10">
             <h3>My Routines</h3>
          </div>
          <div class="col-2" style="padding: 5px">
            <a id="addButton" class="btn btn-secondary" href="r_form.php">Add</a>
          </div>
        </div>
        <br>
          <?php
          $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
          mysqli_select_db($link, 'bjanczuk');

          if (isset($_GET["name"])) {

            if ($stmt = mysqli_prepare($link, "INSERT INTO Routines (creator, name) VALUES (?,?)")) {
                mysqli_stmt_bind_param($stmt, "ss", $_COOKIE["current_user"], $_GET["name"]);
                if (mysqli_stmt_execute($stmt)){
                  $this_rid = mysqli_insert_id($link);

                  if ($stmt1 = mysqli_prepare($link, "INSERT INTO RoutineRatings (username, rid, rating_value, count) VALUES (?, ?, ?, ?)")) {
                    $zero = 0;
                    mysqli_stmt_bind_param($stmt1, "siii", $_COOKIE["current_user"], $this_rid, $zero, $zero);
                    mysqli_stmt_execute($stmt1);
                    mysqli_stmt_close($stmt1);
                  }

                }
                mysqli_stmt_close($stmt);
            }

              $workouts = explode(',', $_GET["workoutArray"]);
              $days = explode(',', $_GET["daysArray"]);
              if ($this_rid && count($workouts) == count($days)) {

                for ($i = 0; $i < count($workouts); ++$i) {
                      if ($stmt1 = mysqli_prepare($link, "INSERT INTO WorkoutsInRoutines (wid, rid, weekdays) VALUES (?,?,?)")) {
                          mysqli_stmt_bind_param($stmt1, "iis", $workouts[$i], $this_rid, $days[$i]);
                          (mysqli_stmt_execute($stmt1));
                          mysqli_stmt_close($stmt1);
                      }
               }
              }
          }
          mysqli_close($link);

        ?>
        <div class="row" id="tableDiv">
          <?php

            $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or die ('Database connection error');
            mysqli_select_db($link, 'bjanczuk');

            $query = "SELECT r.rid, r.creator, r.name, rr.rating_value, rr.count, rh.rating_value as user_rating FROM Routines r INNER JOIN RoutineRatings rr ON r.rid = rr.rid LEFT JOIN RatingHistory rh ON rh.id = r.rid AND rh.username = '".$_COOKIE['current_user']."' AND rh.type = 'routines'";

            $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
  	  	  if ($result->num_rows < 1){
  	  		echo "<h5>No Routines found.  Create one with the add button</h5><br>";
  	  	  } else {
			  echo "<div class='form-group col-sm-8'><input class='form-control' type='text' id='tableSearchInput' placeholder='Search Routines'></div>";
			  echo "<div class='form-group col-sm-4'><select id='allOrUserSelect' class='form-control'><option>All routines</option><option>My routines</option></select></div>";
			  echo "<table class='table' id='routinesTable'>\n";
              echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>Name</th>\n\t\t<th>Current Rating</th>\n\t\t<th>Rate It</th>\n\t\t<th>Workouts</th>\t</tr>\n\t</thead>\n\t<tbody>\n";
			  echo "<tr id='noMatchToSearch' style='display: none'><td style='text-align: center' colspan=5 ><h6>No routines match search</h6></td></tr>";
              while ($tuple = mysqli_fetch_assoc($result)){
                echo "\t<tr>\n";
                
                echo "\t\t<td class=creator> " . $tuple["creator"] . " </td>\n";
                echo "\t\t<td class=name> " . $tuple["name"] . " </td>\n";

                if ($tuple["count"] == "0") { echo "\t\t<td class=rating_value> No Rating </td>\n"; }
                else { echo "\t\t<td class=rating_value> " . number_format(floatval($tuple["rating_value"]), 2) . " / 5.00</td>\n"; }

                // Display 5 stars.
                // Taken from https://jsfiddle.net/BHaTu/ 
                echo "
                <td class='rate_widget'>
                   <ul>
                     <li><a href='#' id='" . $tuple["rid"] . "_1' class='one-star".($tuple["user_rating"] == 1? " checked"  : "")."'>1</a></li>
                     <li><a href='#' id='" . $tuple["rid"] . "_2' class='two-stars".($tuple["user_rating"] == 2? " checked"  : "")."'>2</a></li>
                     <li><a href='#' id='" . $tuple["rid"] . "_3' class='three-stars".($tuple["user_rating"] == 3? " checked"  : "")."'>3</a></li>
                     <li><a href='#' id='" . $tuple["rid"] . "_4' class='four-stars".($tuple["user_rating"] == 4 ? " checked"  : "")."'>4</a></li>
                     <li><a href='#' id='" . $tuple["rid"] . "_5' class='five-stars".($tuple["user_rating"] == 5 ? " checked"  : "")."'>5</a></li>
                   </ul>
			    </td>";


                $curr_id = (int)$tuple["rid"];
                echo "<form action='list_workouts.php' method='get' id='form1'>
                        <input type='hidden' name='workout' value='"."$curr_id"."'>
                        <td><button type='submit' class='btn btn-secondary' id='expandWorkout"."$curr_id"."'>Expand</button></td>
                    </form>";
                echo "\t</tr>\n";
              }
                echo "\t</tbody>\n</table>\n";
            }

            mysqli_free_result($result);
            mysqli_close($link);

          ?>
        </div>
      </div>

      <script>
        $(document).ready(function(){
          // Clear cookies for logout
          $("#logout").click(function(){
            document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
            location.reload();
          });
		  $("#tableSearchInput").keyup(function(){
			searchTable()
		  });
		  $("#allOrUserSelect").change(function(){
			searchTable();
		  });
        });

		function searchTable() {
          let filter = $("#tableSearchInput").val().toLowerCase();
          let current_user = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1]
          let userFilter = ($("#allOrUserSelect").val() == "My routines") ? current_user : ""
		  let numShown = 0;
          $("#routinesTable").find("tr").each(
            function(i){
                creator = $(this).find("td.creator").html();
                if (creator){
                    if (creator.toLowerCase().indexOf(userFilter) < 0){
                        $(this).hide();
                        return true;
                    }
				} else if ($(this).attr('id') == 'noMatchToSearch'){
					return true;
				}
                $(this).find("td").each(
                    function(){
                        if ($(this).html().toLowerCase().indexOf(filter) > -1){
                            $(this).parent().show()
							numShown++;
                            return false;
                        } else {
                            $(this).parent().hide()
                        }
                    }
                );

            });
			if (!numShown){
				$("#noMatchToSearch").show();
			} else {
				$("#noMatchToSearch").hide();
			}
        }
      </script>
    </body>
  </html>
