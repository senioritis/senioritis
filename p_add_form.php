<!DOCTYPE html>
<html>
  <head>
    <title>Workout</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="exercises.php">Exercises</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
          </ul>
      </div>
    </nav>


    <div class="container">
      </br>
      <div class="row">
        <div class ="col-10">
            <a id="backButton" class="btn btn-secondary" href="pastWorkouts.php">back</a>
        </div>
      </div>
      <div class="row">
          <div class='col-sm-6'>
            <h3>Exercises in this Workout:</h3>
              <?php 
                $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
                mysqli_select_db($link, 'bjanczuk');
                $wid = "";
                if (isset($_GET["exercise"])){
                    $wid=$_GET["exercise"];
                } else {
                    header("Status: 404 Not Found");
                    include('error.php');
                    die();
                }
                echo "<div style='display: none' id='wid'>$wid</div>";

                $query = "SELECT e.eid, e.name, e.muscle_group FROM GeneralWorkouts gw, Exercises e, ExercisesInWorkouts ew WHERE gw.wid='"."$wid"."' AND gw.wid = ew.wid AND e.eid = ew.eid";
                $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));


                echo "<table class='table' id='exercisesTable'><tbody>\n";
                echo "\t<thead class='thead-light'>\t<tr>\n\t\t<th>Name</th>\n\t\t<th>Muscle Group</th>\n\t\t<th>Record</th></tr>\n\t</thead>\n";
                while($tuple = mysqli_fetch_assoc($result)) {
                    echo "<tr><td class='name'>".$tuple['name']."</td><td>".$tuple['muscle_group']."</td><td style='display: none' class='eid'>".$tuple['eid']."</td><td><button type='button' class='btn btn-primary recordButton'>Record Reps</button></td></tr>";
                }
                echo "</tbody></table>";
                ?>
                <h6 id="allExercisesRecorded" style='display: none'>All exercises have been recorded.  Press submit to save.</h6>
            </div>
            <div class="col-sm-6">
                <h3>Recorded Exercises:</h3>
                <table class="table" id="recordedExercisesTable">
                    <tbody>
                        <thead class='thead-light'><tr><th>Name</th><th>Sets (reps x weight)</th></tr></thead>
                    </tbody>
                </table>
                <h6 style='text-align: center' id="addSetsMessage">Click "Record Reps" to enter reps and weight for each exercise.</h6>
            </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="setsModal" tabindex="-1" role="dialog" aria-labelledby="setsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="setsModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="modalBody">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" id="modalClose" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="modalNext">Next</button>
              <button type="button" style='display: none' class="btn btn-primary" id="modalSubmitButton">Submit</button>
            </div>
          </div>
        </div>
      </div>
      <form method=post action='p_form.php' id='addForm'>
        <input style='display: none' name='data' id='data'/>
        <input style='display: none' name='wid' id='widInput'/>
        <div class='form-group col-sm-4'>
            <label for='dateInput'>Date:</label>
            <input name='date' type=date class='form-control' id='dateInput'/>
        </div>
        <button type=button class='btn btn-success' id='submitButton' disabled>Submit</button>
      </form>
    </div>
    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
        $(".recordButton").click(function(e){
            resetModal();
            $("#setsModal").modal("show");
            let name = $(this).parent().parent().find(".name").html();
            let eid = $(this).parent().parent().find(".eid").html();
            $("#setsModalLabel").html("Sets for '" + name + "'");
            $("#exerciseName").val(name);
            $("#eid").val(eid);
        });
        $("#modalNext").click(function(e){
            if ($("#numSets").val() <= 0){
                alert("Please enter a number > 0");
                return;
            }
            $("#numSets").hide();
            $("#numSetsLabel").hide();
            $("#modalNext").hide();
            $("#modalSubmitButton").show();
            let numSets = $("#numSets").val();
            $("#repTable").show();
            for (i = 0; i < numSets; i++){
                $("#repTable").append("<tr><td>" + (i+1) + "</td><td><input class='form-control repInput' id='repInput" + i + "' type=number/></td><td><input class='form-control weightInput' id='weightInput" + i + "' type=number/></td></tr>");
            }
        });
        $("#modalSubmitButton").click(function(e){
            let message = "";
            $(".repInput").each(function(e){
                if ($(this).val().length == 0){
                    message = "Please enter reps for all sets";
                    return false;
                }
                if ($(this).val() <= 0){
                    message = "Please enter numbers > 0";
                    return false;
                }
            });
            $(".weightInput").each(function(e){
                if ($(this).val().length == 0){
                    message = "Please enter weight for all sets";
                    return false;
                }
                if ($(this).val() <= 0){
                    message = "Please enter numbers > 0";
                    return false;
                }
            });
            if (message.length){
                alert(message);
                return;
            }
            let reps = $(".repInput").map(function(){ return this.value});
            let weight = $(".weightInput").map(function(){ return this.value});
            $("#recordedExercisesTable").append("<tr><td class='name'>" + $("#exerciseName").val() + "</td><td class='eid' style='display: none' >" + $("#eid").val() + "</td><td class='sets'>" + reps.map(function(i){ return (i+1) + ": " + this + " x " + weight[i];}).get().join('</br>') + "</td></tr>");
            $("#exercisesTable").find(".name:contains(" + $("#exerciseName").val() + ")").parent().remove();

            $("#setsModal").modal("hide");
            $("#addSetsMessage").hide();
            checkDisabled();
            resetModal();
        });
        $("#submitButton").click(function(e){
            let data = {};
            $("#recordedExercisesTable").find("tr").each(function(){
                let text = $(this).find(".sets").html();
                if (text && text.length){
                    let eid = $(this).find(".eid").html();
                    data[eid] = {};
                    text.split('<br>').map((t) => {
                        fields = t.split(' ');
                        let num = fields[0].match(/\d+/);
                        data[eid][num] = {};
                        data[eid][num]['reps'] = fields[1];
                        data[eid][num]['weight'] = fields[3];
                    });
                }
            });
            $("#data").val(JSON.stringify(data));
            $("#widInput").val($("#wid").html());
            $("#addForm").submit();
        });
        $("#dateInput").change(function(){
            checkDisabled();
        });
    });
    
    function checkDisabled(){
        if ($("#exercisesTable").find(".name").length < 1){
            $("#allExercisesRecorded").show();
            if ($("#dateInput").val().length > 0){
                $("#submitButton").attr("disabled", false);
            }
        }
    }
    function resetModal(){
        $("#modalBody").html("\
            <div class='form-group'>\
                    <input style='display: none' id='exerciseName'/>\
                    <input style='display: none' id='eid'/>\
                    <label id='numSetsLabel' for='numSets'>How many sets did you do?</label>\
                    <input class='form-control' id='numSets' type='number'/>\
                </div>\
                <table style='display: none' class='table' id='repTable'>\
                    <tbody><thead class='thead-light'><tr><th>Set</th><th>Reps</th><th>Weight</th></tr></thead></tbody>\
                </table>\
        ");
        $("#modalNext").show();
        $("#modalSubmitButton").hide();
    }
    
    </script>
    </body>
</html>
