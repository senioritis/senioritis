<?php
	$link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
    mysqli_select_db($link, 'bjanczuk');

    // Determine which table will get updated.
    $table_name = $_GET['table'];
    $id_name = ($table_name == "ExerciseRatings" ? "eid" : ($table_name == "RoutineRatings" ? "rid" : "wid"));
    $type_name = ($table_name == "ExerciseRatings" ? "exercises" : ($table_name == "RoutineRatings" ? "routines" : "workouts"));

    // Check if this user's rating is happening on this exercise/workout/routine for the first time, or if it's an update instead.
    $query = "SELECT * FROM RatingHistory WHERE username = '" . $_GET['username'] . "' AND id = " . $_GET[$id_name] . " AND type = '" . $type_name . "'"; 
	$result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
	$previous_rating = -1;
	while ($tuple = mysqli_fetch_assoc($result)) { $previous_rating = $tuple['rating_value']; }

	if ($previous_rating > -1) {
		$queries = array("UPDATE " . $table_name . " SET rating_value = (1.0 * (rating_value * count) + " . $_GET['star_value'] . " - " . $previous_rating . ") / count WHERE " . $id_name . " = " . $_GET[$id_name],

	    				"UPDATE RatingHistory SET rating_value = " . $_GET['star_value'] . " WHERE username = '" . $_GET['username'] . "' AND id = " . $_GET[$id_name] . " AND type = '" . $type_name . "'"
						);
	}
	else {
		$queries = array("UPDATE " . $table_name . " SET rating_value = (1.0 * (rating_value * count) + " . $_GET['star_value'] . ") / (count + 1) WHERE " . $id_name . " = " . $_GET[$id_name],

						"UPDATE " . $table_name . " SET count = count + 1 WHERE " . $id_name .  " = " . $_GET[$id_name],

	    				"INSERT INTO RatingHistory (username, id, type, rating_value) VALUES ('" . $_GET['username'] . "', " . $_GET[$id_name] . ", '" . $type_name . "', " . $_GET['star_value'] . ")"
						);
	}

    foreach ($queries as $query) {
	    $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
	}
?>