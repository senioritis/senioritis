<!DOCTYPE html>
<html>
  <head>
    <title>Senioritis Signup</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class='nav-item'>
            <a class='nav-link' href='login.php'>Login</a>
          </li>
         </ul>
      </div>
    </nav>

    <?php
      $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());

      mysqli_select_db($link, 'bjanczuk');

      if (isset($_POST['name'])){
        if ($stmt = mysqli_prepare($link, "INSERT INTO Users (name, username, email, password, age, weight, height, gender, date_joined) VALUES (?,?,?,?,?,?,?,?, ?)")) {
          mysqli_stmt_bind_param($stmt, "ssssiiiss", $_POST["name"], $_POST["username"], $_POST["email"], password_hash($_POST["password"], PASSWORD_DEFAULT), $_POST["age"], $_POST["weight"], $_POST["height"], $_POST["gender"], date('Y-m-d H:i:s'));
          if (mysqli_stmt_execute($stmt)){
            setcookie('current_user', $_POST['username'], 0, '/');
            header("Location: index.php");
            exit();
          } else{
            echo mysqli_stmt_error($stmt);
          }
          mysqli_stmt_close($stmt);
        }
      }

    mysqli_close($link);
    ?>
    <div class="container">
      <div class="row" stle="text-align: center">
        <div class="col-xs-4">
          <h3>Signup</h3>
          <form method="post" action="">
            Name:<br>
            <input type="text" name="name"/><br>
            Username:<br>
            <input type="text" name="username"/><br>
            Email:<br>
            <input type="email" name="email"/><br>
            Password:<br>
            <input type="password" name="password"/><br>
            Age:<br>
            <input type="number" name="age"/><br>
            Weight(lbs):<br>
            <input type="number" name="weight"/><br>
            Height(inches):<br>
            <input type="number" name="height"/><br>
            Gender:<br>
            <input type="text" name="gender"/><br>
            <br>
            <input type="submit" name="submit" value="Submit" class="btn btn-default">
          </form>
        </div>
      </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function(){
        $("#addButton").click(function(){
          $("#addForm").toggle();
        });
      });
    </script>
  </body>
</html>
