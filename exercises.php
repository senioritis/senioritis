<!DOCTYPE html>
<html>
  <head>
    <title>Exercises</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script>
      $(function(){
            $('.rate_widget a').click(function(){
                // Make sure the chosen star stays selected.
                var star = $(this);
                star.closest('ul').find('.checked').removeClass('checked');
                star.addClass('checked');

                var eid = star.attr('id').split('_')[0];
                var star_value = star.attr('id').split('_')[1];
                var username = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1]
                jQuery.get('update_ratings_table.php', {'eid': parseInt(eid), 'star_value': parseInt(star_value), 'username': username, 'table': 'ExerciseRatings'}, function(d) {
                });
            });
        });
    </script>
    <style>
     .rate_widget ul{
        background: url('Images/star_empty.png') repeat-x;
    }

    .rate_widget a:hover{
        background: url('Images/star_highlight.png') repeat-x;
    }

      .rate_widget a:active,
      .rate_widget a:focus,
      .rate_widget a.checked{
          background: url('Images/star_full.png') repeat-x;
      }
    .rate_widget ul{
        position:relative;
        width:160px; /*5 times the width of your star*/
        height:32px; /*height of your star*/
        overflow:hidden;
        list-style:none;
        margin:0;
        padding:0;
        background-position: left top;
    }

    .rate_widget li{
        display: inline;
    }

    .rate_widget a{
        position:absolute;
        top:0;
        left:0;
        text-indent:-1000em;
        height:32px; /*height of your star*/
        line-height:32px; /*height of your star*/
        outline:none;
        overflow:hidden;
        border: none;
    }

    .rate_widget a.one-star{
        width:20%;
        z-index:6;
    }

    .rate_widget a.two-stars{
        width:40%;
        z-index:5;
    }

    .rate_widget a.three-stars{
        width:60%;
        z-index:4;
    }

    .rate_widget a.four-stars{
        width:80%;
        z-index:3;
    }

    .rate_widget a.five-stars{
        width:100%;
        z-index:2;
    }

    .rate_widget a.current{
        z-index:1;
    }
  	</style>

  </head>
  <body>

  <?php
    if (!isset($_COOKIE['current_user']) || empty($_COOKIE['current_user'])){
      header("Location: login.php");
      exit();
    }
  ?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Workout Planner</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="exercises.php">Exercises <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="workouts.php">Workouts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pastWorkouts.php">Record a Workout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="routines.php">Routines</a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
              <li class='nav-item'>
                <a class='nav-link' id='logout'>Logout</a>
              </li>
  	      </ul>
      </div>
    </nav>

    <br>
    <div class="container">
      <?php
        $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
        mysqli_select_db($link, 'bjanczuk');

        if (isset($_POST["name"])) {

            if ($stmt = mysqli_prepare($link, "INSERT INTO Exercises (creator, name, muscle_group, description) VALUES (?,?,?,?)")) {
                mysqli_stmt_bind_param($stmt, "ssss", $_COOKIE["current_user"], $_POST["name"], $_POST["muscle_dropdown"], $_POST["description"]);
                if (mysqli_stmt_execute($stmt)) {
                    $query = "SELECT eid FROM Exercises ORDER BY eid DESC LIMIT 1";
                    $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
                    while ($tuple = mysqli_fetch_assoc($result)) { 
                        $eid = $tuple['eid']; 
                        $zero = 0;
                        if($stmt1 = mysqli_prepare($link, "INSERT INTO ExerciseRatings (username, eid, rating_value, count) VALUES (?, ?, ?, ?)")) {
                            mysqli_stmt_bind_param($stmt1, "siii", $_COOKIE["current_user"], $eid, $zero, $zero);
                            mysqli_stmt_execute($stmt1);
                            mysqli_stmt_close($stmt1);
                        }
                     }
                } else {
                  echo "Failed to add exercise: " . mysqli_stmt_error($stmt);
                }
                mysqli_stmt_close($stmt);
            }
        }
        $_POST=array();
        mysqli_close($link);

      ?>

      <div class="row">
        <div class="col-10">
          <h3>My Exercises</h3>
        </div>
        <div class="col-2" style="padding: 5px">
          <a id="addButton" class="btn btn-secondary" href="e_form.php">Add</a>
        </div>
      </div>
      <br>
      <div class="row" id="tableDiv">

        <?php
          $link = mysqli_connect('localhost', 'bjanczuk', 'bartosz') or Die(mysqli_connect_error());
          mysqli_select_db($link, 'bjanczuk');
          $query = "SELECT e.eid, e.name, e.muscle_group, e.creator, e.description, er.rating_value, er.count, rh.rating_value as user_rating FROM Exercises e INNER JOIN ExerciseRatings er ON e.eid = er.eid LEFT JOIN RatingHistory rh ON rh.id = e.eid AND rh.username = '".$_COOKIE['current_user']."' AND rh.type = 'exercises'";
          $result = mysqli_query($link, $query) or die('Query failed '. mysqli_error($link));
  	    	if ($result->num_rows < 1){
  	    		echo "<h5>No Exercises found.  Create one with the add button</h5>";
  	    	} else {
				echo "<div class='form-group col-sm-8'><input class='form-control' type='text' id='tableSearchInput' placeholder='Search Exercises'></div>";
				echo "<div class='form-group col-sm-4'><select id='allOrUserSelect' class='form-control'><option>All exercises</option><option>My exercises</option></select></div>";
		        echo "<table class='table' id='exerciseTable'>\n";
                echo "\t<thead class='thead-light'>\t<tr>\n<th>Name</th>\n\t\t<th>Creator</th>\n\t\t<th>Muscle Group</th>\n\t\t<th>Current Rating</th>\n\t\t<th>Description</th>\n\t\t<th>Rate It</th>\n\t</tr>\n\t</thead>\n\t<tbody>\n";
				echo "<tr id='noMatchToSearch' style='display: none'><td style='text-align: center' colspan=5 ><h6>No exercises match search</h6></td></tr>";

                while ($tuple = mysqli_fetch_assoc($result)){
                   echo "\t<tr>\n";

                   echo "\t\t<td class=name> " . $tuple["name"] . " </td>\n";
                   echo "\t\t<td class=creator> " . $tuple["creator"] . " </td>\n";
                   echo "\t\t<td class=muscle_group> " . $tuple["muscle_group"] . " </td>\n";

                   if ($tuple["count"] == "0") { echo "\t\t<td class=rating_value> No Rating </td>\n"; }
                   else { echo "\t\t<td class=rating_value> " . number_format(floatval($tuple["rating_value"]), 2) . " / 5.00</td>\n"; } 
				   #echo $tuple['count'];

                   echo "\t\t<td class=description> " . $tuple["description"] . " </td>\n";


                   // Display 5 stars.
                   // Taken from https://jsfiddle.net/BHaTu/ 
                   echo "
                   <td class='rate_widget'>
                      <ul>
                        <li><a href='#' id='" . $tuple["eid"] . "_1' class='one-star".($tuple["user_rating"] == 1? " checked"  : "")."'>1</a></li>
                        <li><a href='#' id='" . $tuple["eid"] . "_2' class='two-stars".($tuple["user_rating"] == 2? " checked"  : "")."'>2</a></li>
                        <li><a href='#' id='" . $tuple["eid"] . "_3' class='three-stars".($tuple["user_rating"] == 3? " checked"  : "")."'>3</a></li>
                        <li><a href='#' id='" . $tuple["eid"] . "_4' class='four-stars".($tuple["user_rating"] == 4 ? " checked"  : "")."'>4</a></li>
                        <li><a href='#' id='" . $tuple["eid"] . "_5' class='five-stars".($tuple["user_rating"] == 5 ? " checked"  : "")."'>5</a></li>
                      </ul>
					  </td>";

                  echo "\t</tr>\n";
               }
               echo "\t</tbody>\n</table>\n";

			      }

            mysqli_free_result($result);
            mysqli_close($link);
        ?>
      </div>

    </div>

    <script>
      $(document).ready(function(){
        $("#logout").click(function(){
          document.cookie = "current_user =; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
          location.reload();
        });
		$("#tableSearchInput").keyup(function(){
			searchTable();
		});
		$("#allOrUserSelect").change(function(){
			searchTable();
		});
      });

		function searchTable() {
		  let filter = $("#tableSearchInput").val().toLowerCase();
		  let current_user = document.cookie.split(';').filter(cookie => cookie.indexOf('current_user') > -1)[0].split('=')[1]
		  let userFilter = ($("#allOrUserSelect").val() == "My exercises") ? current_user : ""
		  let numShown = 0;
		  $("#exerciseTable").find("tr").each(
			function(i){
				creator = $(this).find("td.creator").html();
				if (creator){
					if (creator.toLowerCase().indexOf(userFilter) < 0){
						$(this).hide();
						return true;
					}
				} else if ($(this).attr('id') == 'noMatchToSearch'){
					return true;
				}
				$(this).find("td").each(
					function(){
						if ($(this).html().toLowerCase().indexOf(filter) > -1){
							$(this).parent().show();
							numShown++;
							return false;
						} else {
							$(this).parent().hide()
						}
					}
				);
				if (!numShown){
					$("#noMatchToSearch").show();
				} else {
					$("#noMatchToSearch").hide();
				}
	
			});
		}
    </script>
  </body>
</html>
